<?php

/** The name of the database for WordPress */
define('DB_NAME', '{{ mariadb.database }}');

/** MySQL database username */
define('DB_USER', '{{ mariadb.user }}');

/** MySQL database password */
define('DB_PASSWORD', '{{ mariadb.password }}');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

{{ wp_salt.stdout }}

$table_prefix = 'wp_';

define('WPLANG', '');

define('WP_DEBUG', false);

/** Disable Automatic Updates Completely */
define( 'AUTOMATIC_UPDATER_DISABLED', false );

/** Define AUTOMATIC Updates for Components */
define( 'WP_AUTO_UPDATE_CORE', true );

/** Absolute path to the WordPress directory */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(_FILE_) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
